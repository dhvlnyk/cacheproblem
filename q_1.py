import time
from time import gmtime, strftime
from operator import itemgetter
import copy
import operator

class LRACacheItem(object):
	"""objects of items stored in cache"""
	def __init__(self, key=None, subject=None, marks=None, nextNodekey=None):
		self.key = key
		self.subject = subject
		self.marks = marks
		self.nextNodekey = nextNodekey

class SortedLruCache:
	def __init__(self, maxsize):
		self.dll = []
		self.maxsize = maxsize

	'''
		method to get a record based on key and updating its timestamp.
	'''
	def getRecord(self,key_value):
		for record in range(0,len(self.dll)):
			if self.dll[record].key == key_value:
				self.dll[record].nextNodekey = time.time()
				break
		return  self.dll[record].key

	def add(self,value):
		key = value.key
		records = self.dll

		temp = LRACacheItem(value.key, value.subject, value.marks, value.nextNodekey)

		value.nextNodekey = time.time()
		'''
			when cache is not full add elements according to sorted order of marks.
		'''
		if self.maxsize > len(self.dll):

			if ( len(self.dll) == 0 ):
				self.dll.append(value)

		else:
			'''
				when cache is full LRU strategy is implemneted and Least recently used element is removed
			'''
			LRUvalue = sorted(self.dll,key=operator.attrgetter('nextNodekey'))[0]
			self.dll.remove(LRUvalue)
		if self.dll[0].marks > temp.marks:
			self.dll.insert(0,temp)
		elif self.dll[-1].marks < temp.marks:
			self.dll.append(temp)
		else:
			dll = []
			dll = copy.deepcopy(self.dll)
			for i in range(1,len(dll)):
				j = i-1
				if dll[j].marks <= temp.marks <= dll[i].marks and len(dll) == len(self.dll):
					self.dll.insert(i,temp)
					break

	def printAll(self):
		for each in self.dll:
			print str(each.key)+","+str(each.subject)+","+str(each.marks)

	def delete(self,key_value):
		dll = []
		dll = copy.deepcopy(self.dll)
		for record in range(0,len(dll)):
			if dll[record].key == key_value:
				del self.dll[record]

	def shutdown(self):
		f = open("output.txt", "w")		
		for each in self.dll:
			f.write(str(each.key)+","+str(each.subject)+","+str(each.marks))
			f.write("\n")
		f.close()	


f = open("test_dataset.txt")
cache = SortedLruCache(maxsize=20)
for each in f:
	each = each.strip();
	fields = each.split(",")
	record = LRACacheItem(int(fields[0]),fields[1],int(fields[-1]),time.time())
	cache.add(record)

print "The last added record id is " + str(cache.getRecord(record.key))

record_one = LRACacheItem(7,"arts",10,time.time())
cache.add(record_one)
record_two = LRACacheItem(999,"commerce",80,time.time())
cache.add(record_two)
print "Adding New records having ids: " + str(record_one.key) + "and " + str(record_two.key)
cache.printAll()

print "Deleting Newly added record having id " + str(record_two.key)
cache.delete(record_two.key)
cache.printAll()

cache.shutdown()